﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.IdentityModel.Selectors;


namespace CertificateManager
{
    public class ClientCertificateValidator : X509CertificateValidator
    {
        public override void Validate(X509Certificate2 certificate)
        {
            if (certificate.Subject.Equals(certificate.Issuer))
            {
                throw new Exception("Certificate is self-issued.");
            }
        }
    }
}