﻿using LoggerUtility;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;

public class XmlLogger : ILog
{
    public void NewLog(LogData logData)
    {
        var path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\FsmLog.txt";

        if (!File.Exists("FSMLog.xml"))
        {
            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
            xmlWriterSettings.Indent = true;
            xmlWriterSettings.NewLineOnAttributes = true;
            using (XmlWriter xmlWriter = XmlWriter.Create("FSMLog.xml", xmlWriterSettings))
            {
                xmlWriter.WriteStartDocument();
                xmlWriter.WriteStartElement("FSMEvents");

                xmlWriter.WriteStartElement("Event");
                xmlWriter.WriteElementString("Message", logData.MessageToLog);
                xmlWriter.WriteElementString("Severity", logData.MessageSeverity.ToString());
                xmlWriter.WriteEndElement();

                xmlWriter.WriteEndElement();
                xmlWriter.WriteEndDocument();
                xmlWriter.Flush();
                xmlWriter.Close();
            }
        }
        else
        {
            XDocument xDocument = XDocument.Load("FSMLog.xml");
            XElement root = xDocument.Element("FSMEvents");
            IEnumerable<XElement> rows = root.Descendants("Event");
            XElement lastRow = rows.Last();
            lastRow.AddAfterSelf(
               new XElement("Event",
               new XElement("Message", logData.MessageToLog),
               new XElement("Severity", logData.MessageSeverity.ToString())));
            xDocument.Save("FSMLog.xml");
        }
    }
}