﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace LoggerUtility
{
    public class Logger
    {
		ILog LoggerComponent;



		public Logger()
		{
			string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\Log.dll";
			LoadDll(path);
		}

		public void AddNewLog(LogData logData)
        {
            LoggerComponent.NewLog(logData);
			ConsoleLogger.Log(logData.MessageToLog, logData.MessageSeverity);
        }

        void LoadDll(string path)
        {
            var assembly = Assembly.LoadFile(path);
            var types = assembly.GetTypes();
            var functions = new List<ILog>();

            foreach (var item in types)
            {
                if (!item.IsInterface && !item.IsAbstract)
                {
                    LoggerComponent = Activator.CreateInstance(item) as ILog;
                    return;
                }
            }

            if (LoggerComponent == null)
            {
                throw new Exception("Loger component dll not found");
            }
        }
    }
}
