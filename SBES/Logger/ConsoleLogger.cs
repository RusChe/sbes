﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerUtility
{
	public static class ConsoleLogger
	{
		private static Severity filterLogLevel = Severity.Debug;



		public static void Log(string message, Severity logLevel)
		{
			// Check if request passes filter
			if (logLevel < filterLogLevel)
				return;

			// Set console color according to log level
			switch (logLevel)
			{
				case Severity.Debug:
					Console.ForegroundColor = ConsoleColor.White;
					Console.BackgroundColor = ConsoleColor.DarkGreen;
					break;
				case Severity.Info:
					Console.ForegroundColor = ConsoleColor.DarkGreen;
					break;
				case Severity.Warning:
					Console.ForegroundColor = ConsoleColor.Yellow;
					break;
				case Severity.Error:
					Console.ForegroundColor = ConsoleColor.Red;
					break;
				case Severity.Critical:
					Console.ForegroundColor = ConsoleColor.White;
					Console.BackgroundColor = ConsoleColor.Red;
					break;
			}

			// Print the message
			Console.WriteLine($"{DateTime.Now} [{logLevel.ToString().ToUpper()}]: {message}");

			// Reset console colors
			Console.ForegroundColor = ConsoleColor.Gray;
			Console.BackgroundColor = ConsoleColor.Black;
		}

		/// <summary>
		/// Set log level for ConsoleLogger.
		/// After this function, following Log calls that have logLevel lower than the one set, will not be printed.
		/// </summary>
		/// <param name="logLevel"></param>
		public static void SetLogLevel(Severity logLevel)
		{
			filterLogLevel = logLevel;
		}
	}
}