﻿using CertificateManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace EndClient
{
    public class WCFClient : ChannelFactory<IFileContract>, IFileContract
    {
        IFileContract proxy;



        public WCFClient(NetTcpBinding binding, EndpointAddress address)
            : base(binding, address)
        {
            /// cltCertCN.SubjectName should be set to the client's username. .NET WindowsIdentity class provides information about Windows user running the given process
            var username = Formatter.ParseName(WindowsIdentity.GetCurrent().Name).Split('_');
            string cltCertCN = username[1];
            Console.WriteLine(username[1]);
            //string cltCertCN = "fsmclient";

            this.Credentials.ServiceCertificate.Authentication.CertificateValidationMode = System.ServiceModel.Security.X509CertificateValidationMode.Custom;
            this.Credentials.ServiceCertificate.Authentication.CustomCertificateValidator = new ClientCertificateValidator();
            this.Credentials.ServiceCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;

            /// Set appropriate client's certificate on the channel. Use CertManager class to obtain the certificate based on the "cltCertCN"
            this.Credentials.ClientCertificate.Certificate = CertificateUtility.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, cltCertCN);

            proxy = this.CreateChannel();
        }



        public void CreateFile(string folderName)
        {
            try
            {
                proxy.CreateFile(folderName);
            }
            catch (Exception e)
            {
                Console.WriteLine("[SendMessage] ERROR = {0}", e.Message);
            }
        }

        public void CreateFolder(string folderName)
        {
            try
            {
                proxy.CreateFolder(folderName);
            }
            catch (Exception e)
            {
                Console.WriteLine("[SendMessage] ERROR = {0}", e.Message);
            }
        }

        public void DeleteFile(string folderName)
        {
            try
            {
                proxy.DeleteFile(folderName);
            }
            catch (Exception e)
            {
                Console.WriteLine("[SendMessage] ERROR = {0}", e.Message);
            }
        }

        public void DeleteFolder(string folderName)
        {
            try
            {
                proxy.DeleteFolder(folderName);
            }
            catch (Exception e)
            {
                Console.WriteLine("[SendMessage] ERROR = {0}", e.Message);
            }
        }

        public void UpdateFile(string folderName, string content)
        {
            try
            {
                proxy.UpdateFile(folderName, content);
            }
            catch (Exception e)
            {
                Console.WriteLine("[SendMessage] ERROR = {0}", e.Message);
            }
        }
    }
}
