﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EndClient
{
    class Program
    {
        static void Main(string[] args)
        {
            string port = "11111";
            if (args.Length > 0)
            {
                port = args[0];

            }
            else
            {
                Console.WriteLine("Enter port:");
                port = Console.ReadLine();
            }
            Connection connection = null;
            int i = 0;

            try
            {
                connection = new Connection(port);

            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to connect to FSM server.");
                Console.WriteLine(e);
                return;
            }
            if (args.Length > 0)
            {
                connection.proxy.CreateFolder("Folder_1");
                connection.proxy.DeleteFolder("Folder_1");
                connection.proxy.CreateFile("File_1.txt");
                connection.proxy.UpdateFile("File_1.txt", "Update");
                connection.proxy.DeleteFile("File_1.txt");
            }
            do
            {
                //Console.Clear();
                i = Menu();

                switch (i)
                {
                    case 1:
                        connection.proxy.CreateFolder("Folder_1");
                        break;
                    case 2:
                        connection.proxy.DeleteFolder("Folder_1");
                        break;
                    case 3:
                        connection.proxy.CreateFile("File_1.txt");
                        break;
                    case 4:
                        connection.proxy.UpdateFile("File_1.txt", "Update");
                        break;
                    case 5:
                        connection.proxy.DeleteFile("File_1.txt");
                        break;
                }
            } while (i != 0);

            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }

        private static int Menu()
        {
            int choice = -1;
            do
            {
                Console.WriteLine("1. Create a folder");
                Console.WriteLine("2. Delete a folder");
                Console.WriteLine("3. Create a file");
                Console.WriteLine("4. Update a file");
                Console.WriteLine("5. Delete a file");
                Console.WriteLine("0. Exit application");

                int.TryParse(Console.ReadLine(), out choice);
            } while (choice < 0 || choice > 5);

            return choice;
        }
    }
}
