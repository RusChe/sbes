﻿using CertificateManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;
using System.Threading.Tasks;

namespace EndClient
{
    public class Connection
    {
        string srvCertCN = "fsmservice";
        public WCFClient proxy;
        NetTcpBinding binding;



        public Connection(string port)
        {
            binding = new NetTcpBinding();
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;

            /// Use CertManager class to obtain the certificate based on the "srvCertCN" representing the expected service identity.
            X509Certificate2 srvCert = CertificateUtility.GetCertificateFromStorage(StoreName.TrustedPeople, StoreLocation.LocalMachine, srvCertCN);
            EndpointAddress address = new EndpointAddress(new Uri($"net.tcp://127.0.0.1:{port}/r"),
                                      new X509CertificateEndpointIdentity(srvCert));

            proxy = new WCFClient(binding, address);
        }

    }
}