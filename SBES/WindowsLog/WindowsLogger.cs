﻿using LoggerUtility;
using System;
using System.Diagnostics;

public class WindowsLogger : ILog
{
    private EventLog customLog = null;
    const string SourceName = "FSMLogger";
    const string LogName = "FSMEvent";

    public WindowsLogger()
    {
        try
        {
            customLog = new EventLog(LogName, Environment.MachineName, SourceName);
        }
        catch (Exception e)
        {
            customLog = null;
            Console.WriteLine("Error while trying to create log handle. Error = {0}", e.Message);
        }
    }

    public void NewLog(LogData logData)
    {
        if (customLog != null)
        {
            customLog.WriteEntry(logData.MessageToLog, SeverityToEventLogEntryType(logData.MessageSeverity));
        }
        else
        {
            throw new ArgumentException(string.Format("Error while trying to write event to event log."));
        }
    }

	private static EventLogEntryType SeverityToEventLogEntryType(Severity severity)
	{
		switch (severity)
		{
			case Severity.Debug:
			case Severity.Info:
				return EventLogEntryType.Information;
			case Severity.Warning:
				return EventLogEntryType.Warning;
			case Severity.Critical:
			case Severity.Error:
				return EventLogEntryType.Error;
			default:
				return EventLogEntryType.Information;
		}
	}
}