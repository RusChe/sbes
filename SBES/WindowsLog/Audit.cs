﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;


public class Audit : IDisposable
{

    private static EventLog customLog = null;
    const string SourceName = "FSMLog.Audit";
    const string LogName = "FSMLogger";

    static Audit()
    {
        //try
        //{
        //    //if (!EventLog.SourceExists(SourceName))
        //    //{
        //    //   // EventLog.CreateEventSource(SourceName, LogName);
        //    //}

        //   // customLog = new EventLog(LogName, Environment.MachineName, SourceName);
        //}
        //catch (Exception e)
        //{
        //    //customLog = null;
        //    //Console.WriteLine("Error while trying to create log handle. Error = {0}", e.Message);
        //}
    }
    public static void NewEvent(LogData logData)
    {
        //if (customLog != null)
        //{
        //    customLog.WriteEntry(logData.MessageToLog, EventLogEntryType.Error);
        //}
        //else
        //{
        //    throw new ArgumentException(string.Format("Error while trying to write event to event log."));
        //}
    }

    //public static void AuthenticationSuccess(string userName)
    //{
    //          string UserAuthenticationSuccess = AuditEvents.UserAuthenticationSuccess;
    //          if (customLog != null)
    //	{
    //              string message = string.Format(UserAuthenticationSuccess, userName);
    //              customLog.WriteEntry(message);
    //          }
    //	else
    //	{
    //		throw new ArgumentException(string.Format("Error while trying to write event (eventid = {0}) to event log.", (int)AuditEventTypes.UserAuthenticationSuccess));
    //	}
    //}

    //public static void AuthorizationSuccess(string userName, string serviceName)
    //{
    //          string UserAuthorizationSuccess = AuditEvents.UserAuthorizationSuccess;
    //          if (customLog != null)
    //          {
    //              string message = string.Format(UserAuthorizationSuccess, userName, serviceName);
    //              customLog.WriteEntry(message);
    //          }
    //          else
    //          {
    //              throw new ArgumentException(string.Format("Error while trying to write event (eventid = {0}) to event log.", (int)AuditEventTypes.UserAuthorizationSuccess));
    //          }
    //      }

    ///// <summary>
    ///// 
    ///// </summary>
    ///// <param name="userName"></param>
    ///// <param name="serviceName"> should be read from the OperationContext as follows: OperationContext.Current.IncomingMessageHeaders.Action</param>
    ///// <param name="reason">permission name</param>
    //public static void AuthorizationFailed(string userName, string serviceName, string reason)
    //{
    //          string UserAuthorizationFailed = AuditEvents.UserAuthorizationFailed;
    //          if (customLog != null)
    //          {
    //              string message = string.Format(UserAuthorizationFailed, userName, serviceName, reason);
    //              customLog.WriteEntry(message, EventLogEntryType.Error);
    //          }
    //          else
    //          {
    //              throw new ArgumentException(string.Format("Error while trying to write event (eventid = {0}) to event log.", (int)AuditEventTypes.UserAuthorizationFailed));
    //          }
    //      }

    public void Dispose()
    {
        if (customLog != null)
        {
            customLog.Dispose();
            customLog = null;
        }
    }
}
