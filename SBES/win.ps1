#create folders

md -Force "C:\SBES_EXE\SysServer"
md -Force "C:\SBES_EXE\FSM_win"
md -Force "C:\SBES_EXE\EndClient" 

#copy to annother location , Dino set up your project locations and Enjoy !
Copy-Item "C:\Development\SBES\SBES\SysLogServer\bin\Debug\*" -Destination "C:\SBES_EXE\SysServer"
Copy-Item "C:\Development\SBES\SBES\TxtLog\bin\Debug\Log.dll" -Destination "C:\SBES_EXE\SysServer"
Copy-Item "C:\Development\SBES\SBES\FSM\bin\Debug\*" -Destination "C:\SBES_EXE\FSM_win"
Copy-Item "C:\Development\SBES\SBES\WindowsLog\bin\Debug\Log.dll" -Destination "C:\SBES_EXE\FSM_win"
Copy-Item "C:\Development\SBES\SBES\EndClient\bin\Debug\*" -Destination "C:\SBES_EXE\EndClient"


# Run all apps
$username = "syslog"
$password = "1111"

$securePassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $securePassword
Start-Process -FilePath "C:\SBES_EXE\SysServer\SysLogServer.exe"  -WorkingDirectory "C:\SBES_EXE\SysServer" -Credential $credential

$username = "fsmservice"
$password = "1111"

$securePassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $securePassword
Start-Process -FilePath "C:\SBES_EXE\FSM_win\FSM.exe"  -WorkingDirectory "C:\SBES_EXE\FSM_win" -ArgumentList "11111" -Credential $credential

$username = "fsmclient"
$password = "1111"

$securePassword = ConvertTo-SecureString $password -AsPlainText -Force
$credential = new-object -typename System.Management.Automation.PSCredential -argumentlist $username, $securePassword
Start-Process -FilePath "C:\SBES_EXE\EndClient\EndClient.exe"  -WorkingDirectory "C:\SBES_EXE\EndClient" -ArgumentList "11111" -Credential $credential
