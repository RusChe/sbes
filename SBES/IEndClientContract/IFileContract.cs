﻿using System.ServiceModel;

[ServiceContract]
public interface IFileContract
{
    [OperationContract]
    void CreateFolder(string folderName);

    [OperationContract]
    void DeleteFolder(string folderName);

    [OperationContract]
    void CreateFile(string folderName);
    [OperationContract]
    void UpdateFile(string folderName, string content);
    [OperationContract]
    void DeleteFile(string folderName);
}