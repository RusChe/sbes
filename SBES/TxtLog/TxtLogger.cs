﻿using System;
using System.Diagnostics;
using System.IO;
using LoggerUtility;

public class TxtLogger : ILog
{
    public void NewLog(LogData logData)
    {
        var path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + @"\FsmLog.txt";
        File.AppendAllText(path, logData.ToString());
    }
}