﻿using System;
using System.ServiceModel;
using SyslogContracts;
using LoggerUtility;

namespace SysLogClient
{
    public class SyslogClient : ChannelFactory<ISyslogServices>, ISyslogServices, IDisposable
    {
        ISyslogServices proxy;

        public SyslogClient(EndpointAddress address)
            : base(new NetTcpBinding(), address)
        {
            proxy = CreateChannel();
        }

        public void Log(LogData entry)
        {
            try
            {
                proxy.Log(entry);
                Console.WriteLine("Log entry sent to Syslog server.");
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to send log entry to Syslog server. Error message: " + e.Message);
            }
        }

        public void Dispose()
        {
            if (proxy != null)
                proxy = null;

            Close();
        }
    }
}
