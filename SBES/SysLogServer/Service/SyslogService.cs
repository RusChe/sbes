﻿using System;
using System.Security.Permissions;
using System.ServiceModel;
using System.Threading;
using SyslogContracts;
using LoggerUtility;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace SysLogServer
{
	class SyslogService : ISyslogServices
	{
		private static Logger logger;
        private static object logLock = new object();
        private Dictionary<string, List<DateTime>> fileActions = new Dictionary<string, List<DateTime>>();

		private const int N = 10;
		private const int M = 3;



		public SyslogService()
		{
			logger = new Logger();
		}

		[PrincipalPermission(SecurityAction.Demand, Role = "Connect")]
		public void Log(LogData entry)
		{
            lock (logLock)
            {
                logger.AddNewLog(entry);

                if (Thread.CurrentPrincipal.IsInRole("Monitor"))
                {
                    if (entry.MessageSeverity >= Severity.Error)
                    { 
                        string fileName = Regex.Match(entry.MessageToLog, "\"([^\"]*)\"").Value;

                        if (!fileActions.ContainsKey(fileName))
                            fileActions.Add(fileName, new List<DateTime>());

                        fileActions[fileName].Add(DateTime.Now);

                        List<DateTime> actionTimestamps = fileActions[fileName];
                        if (actionTimestamps.Count == M)
                        {
                            // compare 1st and Mth timestamp
                            double seconds = (actionTimestamps[actionTimestamps.Count - 1] - actionTimestamps[0]).TotalSeconds;
                            if (seconds <= N)
                            {
                                double roundedSeconds = ((int)(seconds * 100)) / 100.0;

                                string logMsg = $"{M} FAILED ATTEMPTS RECIEVED FROM MONITOR \"{Thread.CurrentPrincipal.Identity.Name.ToUpper()}\" (TIME DELTA: {roundedSeconds}s, MAX IS {N}s)";
                                LogData logData = new LogData() { MessageToLog = logMsg, MessageSeverity = Severity.Critical };
                                logger.AddNewLog(logData);

                                actionTimestamps.Clear();
                            }
                            else
                            {
                                actionTimestamps.RemoveAt(0);
                            }
                        }
                    }
                }
            }
        }
	}
}
