﻿using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace SysLogServer
{
	class SyslogPrincipal : IPrincipal
	{
		WindowsIdentity identity = null;



		public SyslogPrincipal(WindowsIdentity windowsIdentity)
		{
			identity = windowsIdentity;
		}

		public IIdentity Identity
		{
			get { return identity; }
		}

		public bool IsInRole(string permission)
		{
			foreach (IdentityReference group in identity.Groups)
			{
				SecurityIdentifier sid = (SecurityIdentifier)group.Translate(typeof(SecurityIdentifier));
				IdentityReference name = sid.Translate(typeof(NTAccount));
				string groupName = Formatter.ParseName(name.ToString());

				string[] permissions = RolesConfig.GetPermissions(groupName);
				if (permissions != null)
				{
					foreach (string perm in permissions)
					{
						if (perm.Equals(permission))
						{
							return true;
						}
					}
				}
			}

			return false;
		}
	}
}
