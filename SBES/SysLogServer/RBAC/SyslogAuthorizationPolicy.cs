﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Claims;
using System.IdentityModel.Policy;
using System.Security.Principal;

namespace SysLogServer
{
	class SyslogAuthorizationPolicy : IAuthorizationPolicy
	{

		public SyslogAuthorizationPolicy()
		{
			Id = Guid.NewGuid().ToString();
		}

		public string Id { get; }
		public ClaimSet Issuer
		{
			get { return ClaimSet.System; }
		}

		public bool Evaluate(EvaluationContext evaluationContext, ref object state)
		{
			if (!evaluationContext.Properties.TryGetValue("Identities", out object list))
				return false;

			IList<IIdentity> identities = list as IList<IIdentity>;
			if (list == null || identities.Count <= 0)
				return false;

			evaluationContext.Properties["Principal"] = new SyslogPrincipal((WindowsIdentity)identities[0]);

			return true;
		}
	}
}
