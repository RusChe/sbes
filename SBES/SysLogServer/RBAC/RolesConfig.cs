﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysLogServer
{
	class RolesConfig
	{
		public static string[] GetPermissions(string rolename)
		{
			string permissionString = (string)RBAC.SyslogRoles.ResourceManager.GetObject(rolename);

			if (permissionString != null)
				return permissionString.Split(',');

			return null;
		}
	}
}
