﻿using System;
using System.ServiceModel;

namespace SysLogServer
{
	/// <summary>
	/// Klasa koja se koristi kad postoji zajednicki preduslov za pristup svim metodama servisa.
	/// </summary>
	class SyslogAuthorizationManager : ServiceAuthorizationManager
	{
		protected override bool CheckAccessCore(OperationContext operationContext)
		{
			SyslogPrincipal principal = operationContext.ServiceSecurityContext.AuthorizationContext.Properties["Principal"] as SyslogPrincipal;
			return principal.IsInRole("Connect");
		}
	}
}
