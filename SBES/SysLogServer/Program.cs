﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Policy;
using System.ServiceModel;
using System.ServiceModel.Description;
using SyslogContracts;

namespace SysLogServer
{
    class Program
    {
        static void Main(string[] args)
        {
			ServiceHost host = new ServiceHost(typeof(SyslogService));
			host.AddServiceEndpoint(typeof(ISyslogServices), new NetTcpBinding(), "net.tcp://localhost:9000/SyslogService");

			host.Description.Behaviors.Remove(typeof(ServiceDebugBehavior));
			host.Description.Behaviors.Add(new ServiceDebugBehavior() { IncludeExceptionDetailInFaults = true });

			host.Authorization.ServiceAuthorizationManager = new SyslogAuthorizationManager();

			host.Authorization.PrincipalPermissionMode = PrincipalPermissionMode.Custom;
			List<IAuthorizationPolicy> policies = new List<IAuthorizationPolicy>();
			policies.Add(new SyslogAuthorizationPolicy());
			host.Authorization.ExternalAuthorizationPolicies = policies.AsReadOnly();

			host.Open();
			Console.WriteLine("Syslog server is open. Press any key to close.");
			Console.ReadKey();
			host.Close();
        }
    }
}
