﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SysLogServer
{
	class Formatter
	{
		public static string ParseName(string winLogonName)
		{
			if (winLogonName.Contains("@"))
				return winLogonName.Split('@')[0];
			else if (winLogonName.Contains("\\"))
				return winLogonName.Split('\\')[1];
			else
				return winLogonName;
		}
	}
}
