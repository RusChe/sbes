using System;
using System.Runtime.Serialization;

namespace LoggerUtility
{
	public enum Severity
	{
		Debug,
		Info,
		Warning,
		Error,
		Critical
	}

	[DataContract]
	public class LogData
	{
		[DataMember]
		public string MessageToLog;

		[DataMember]
		public Severity MessageSeverity;

		public LogData() { }

		public LogData(string message, Severity severity)
		{
			this.MessageToLog = message;
			this.MessageSeverity = severity;
		}

		public override string ToString()
		{
			return $"[{MessageSeverity}] {MessageToLog}" + Environment.NewLine;
		}
	}
}