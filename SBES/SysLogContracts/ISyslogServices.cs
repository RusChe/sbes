﻿using System.ServiceModel;
using LoggerUtility;

namespace SyslogContracts
{
    [ServiceContract]
    public interface ISyslogServices
    {
        [OperationContract]
        void Log(LogData data);
    }
}
