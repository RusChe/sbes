﻿using CertificateManager;
using LoggerUtility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Security.Principal;
using System.ServiceModel;
using System.ServiceModel.Security;
using System.Text;
using System.Threading.Tasks;

namespace FSM
{
    public class FSMEndClientService
    {
        ServiceHost host;
        public FSMEndClientService(string port)
        {
            /// srvCertCN.SubjectName should be set to the service's username. .NET WindowsIdentity class provides information about Windows user running the given process
            var username = Formatter.ParseName(WindowsIdentity.GetCurrent().Name).Split('_');
            string srvCertCN = username[1];
            //string srvCertCN = "fsmservice";

            NetTcpBinding binding = new NetTcpBinding();
            binding.Security.Transport.ClientCredentialType = TcpClientCredentialType.Certificate;
            binding.ReceiveTimeout = new TimeSpan(0, 1, 0);
            binding.MaxBufferSize = 4096;
            binding.MaxReceivedMessageSize = 4096;

            string address = $"net.tcp://127.0.0.1:{port}/r";
            host = new ServiceHost(typeof(WCFService));
            host.AddServiceEndpoint(typeof(IFileContract), binding, address);

            host.Credentials.ClientCertificate.Authentication.CertificateValidationMode = X509CertificateValidationMode.Custom;
            host.Credentials.ClientCertificate.Authentication.CustomCertificateValidator = new ServiceCertificateValidator();

            ///If CA doesn't have a CRL associated, WCF blocks every client because it cannot be validated
            host.Credentials.ClientCertificate.Authentication.RevocationMode = X509RevocationMode.NoCheck;

            ///Set appropriate service's certificate on the host. Use CertManager class to obtain the certificate based on the "srvCertCN"
            host.Credentials.ServiceCertificate.Certificate = CertificateUtility.GetCertificateFromStorage(StoreName.My, StoreLocation.LocalMachine, srvCertCN);
            /// host.Credentials.ServiceCertificate.Certificate = CertManager.GetCertificateFromFile("WCFService.pfx");

            try
            {
                host.Open();
                Console.WriteLine("WCFService is started.\nPress <enter> to stop ...");
            }
            catch (Exception e)
            {
                Console.WriteLine("[ERROR] {0}", e.Message);
                Console.WriteLine("[StackTrace] {0}", e.StackTrace);
                host.Close();
            }

        }
        public void CloseService()
        {
            host.Close();
        }
    }
}