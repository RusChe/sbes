﻿using LoggerUtility;
using SysLogClient;
using System;
using System.IO;
using System.ServiceModel;

namespace FSM
{
    public class WCFService : IFileContract
    {
        private Logger logger;
		private SyslogClient syslogProxy;



        /// Wcf service constructor is called at first connction request
        public WCFService()
        {
            logger = new Logger();
			syslogProxy = new SyslogClient(new EndpointAddress(new Uri("net.tcp://localhost:9000/SyslogService")));
		}

        public void CreateFolder(string folderName)
        {
			LogData logData;

			if (!Directory.Exists(folderName))
			{
				Directory.CreateDirectory(folderName);

				logData = new LogData($"Created folder \"{folderName}\"", Severity.Info);
			}
			else
			{
				logData = new LogData($"Folder \"{folderName}\" already exists.", Severity.Error);
			}

			logger.AddNewLog(logData);
			syslogProxy.Log(logData);
		}

		public void SetAccessRights(string folderName)
		{
			// do some stuff

			//logger.AddNewLog($"Set accsess rights to folder with name \"{folderName}\"", Severity.Warning);
		}

		public void DeleteFolder(string folderName)
		{
			LogData logData;

			if (Directory.Exists(folderName))
			{
				Directory.Delete(folderName);

				logData = new LogData($"Deleted folder \"{folderName}\"", Severity.Warning);
			}
			else
			{
				logData = new LogData($"Folder \"{folderName}\" does not exist.", Severity.Error);
			}

			logger.AddNewLog(logData);
			syslogProxy.Log(logData);
		}

		public void CreateFile(string fileName)
		{
			LogData logData;

			if (!File.Exists(fileName))
			{
				File.Create(fileName).Dispose();

				logData = new LogData($"Created file \"{fileName}\"", Severity.Info);
			}
			else
			{
				logData = new LogData($"File \"{fileName}\" already exists.", Severity.Error);
			}

			logger.AddNewLog(logData);
			syslogProxy.Log(logData);
		}

		public void UpdateFile(string fileName, string content)
		{
			LogData logData;

			if (File.Exists(fileName))
			{
				File.WriteAllText(fileName, content);

				logData = new LogData($"Updated file \"{fileName}\"", Severity.Info);
			}
			else
			{
				logData = new LogData($"File \"{fileName}\" does not exist", Severity.Error);
			}

			logger.AddNewLog(logData);
			syslogProxy.Log(logData);
		}

		public void DeleteFile(string fileName)
		{
			LogData logData;

			if (File.Exists(fileName))
			{
				File.Delete(fileName);

				logData = new LogData($"Deleted file \"{fileName}\"", Severity.Warning);
			}
			else
			{
				logData = new LogData($"File \"{fileName}\" does not exist.", Severity.Error);
			}

			logger.AddNewLog(logData);
			syslogProxy.Log(logData);
		}
    }
}
