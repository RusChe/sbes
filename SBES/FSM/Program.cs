﻿using CertificateManager;
using LoggerUtility;
using System;

namespace FSM
{
    class Program
    {
        static void Main(string[] args)
        {
            string port = "11111";
            if (args.Length > 0)
                port = args[0];
            FSMEndClientService service = null;
            try
            {
                service = new FSMEndClientService(port);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
            service.CloseService();
        }
    }
}
